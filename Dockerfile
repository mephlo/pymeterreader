FROM python:3.11-slim-bookworm

RUN apt-get update && apt install -y git && apt-get clean
RUN python3 -m venv /opt/venv

# Including repo folder
ADD ./ /opt/pymeterreader/
RUN . /opt/venv/bin/activate && pip install /opt/pymeterreader/
CMD . /opt/venv/bin/activate && exec python /opt/pymeterreader/pymeterreader/meter_reader.py
